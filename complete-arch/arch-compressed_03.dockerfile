FROM alpine:edge

LABEL distro "Arch Linux"
LABEL description "Each split compressed file for Docker to pull with Git Bash and WSL limiting in maximun 1.5 GB"
LABEL tag "Compressed"
LABEL version "2.1.0"
LABEL maintainer "Gustavo Costa <gusbemacbe@gmail.com>"
LABEL vendor "Gustavo Costa"

COPY wsl/arch/arch-complete.tar.partac /home/

RUN ls -al /home/

CMD ["sh"]