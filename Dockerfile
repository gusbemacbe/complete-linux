FROM chko/docker-pushrm:1
COPY entrypoint.sh Dockerfile complete-alpine/README.md /
ENTRYPOINT [ "/entrypoint.sh" ]
