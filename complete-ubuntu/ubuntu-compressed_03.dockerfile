FROM alpine:edge

LABEL distro "Ubuntu Linux"
LABEL description "Apps and configs for itubers"
LABEL tag "Beta"
LABEL version "2.1.0"
LABEL maintainer "Gustavo Costa <gusbemacbe@gmail.com>"
LABEL vendor "Gustavo Costa" 

COPY wsl/ubuntu/ubuntu-complete.tar.partac /home/

RUN ls -al /home/

CMD ["sh"]