# Complete Linux

<details>
  <summary>Português</summary>
  
  As imegns mínima, média e completa de distirbuições Alpine Linux, Arch Linux, Ubuntu e Void Linux são designadas para atender as necessidades dos desenvolvedores, engenheiros e programadores da empresa que podem puxá-las a partir do Docker, sob a autorização da empresa, para exportar do Docker e importar para o WSL. 

  As etiquetas (_tags_) das imagens – `minimal`, `middle` e `complete` – dependem do que o colaborador quiser ou precisar. Você pode visualizar a tabela de comparações entre etiquetas e ver quais pacotes cada etiqueta têm ou não têm o você precisa. [Visite aqui](./tables.md)

  ### Qual a sua escolha certa?

  Você pode verificra sem visitar a tabela grande, mas colocaremos uma lista mínima de aplicativos, bibliotecas e pacotes.

  #### `minimal`

  - Vem com Bash e ZSH
  - Vem com `curl`, `git`, `openssh`, `wget` e `xclip`
  - Vem com funções de Bash e ZSH: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` e `tree`
  - Vem com comandos de comprimir e extrair: `7zip`, `tar`, `unzip`, `zip` e `zstd`
  - Vem com funções de ZSH: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` e `slurp`
  - Vem com Neofetch e todas as suas dependências, Powerlevel10k e Znap
  - Vem com editores de texto no terminal: `micro` e `neovim`
  - Vem com `pandoc`
  - Vem com linguagens C, C++, Lua e Python
  - Outras coisas: `htop`, `pip` e `pipes.sh`

  #### `middle`

  - Vem com Bash e ZSH
  - Vem com `curl`, `git`, `openssh`, `wget` e `xclip`
  - Vem com funções de Bash e ZSH: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` e `tree`
  - Vem com comandos de comprimir e extrair: `tar`, `unzip`, `zip` e `zstd`
  - Vem com funções de ZSH: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` e `slurp`
  - Vem com Neofetch e todas as suas dependências, Oh My Posh, Powerlevel10k e Znap
  - Vem com editores de texto no terminal: `micro` e `neovim`
  - Vem com `pandoc`
  - Vem com linguagens C, C++, Lua, Node.js (NVM) e Python
  - Vem com Kitty
  - Outras coisas: `chameolon`, `htop`, `pip` e `pipes.sh`

  #### `complete`

  - Vem com Bash e ZSH
  - Vem com `curl`, `git`, `openssh`, `wget` e `xclip`
  - Vem com funções de Bash e ZSH: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` e `tree`
  - Vem com comandos de comprimir e extrair: `tar`, `unzip`, `zip` e `zstd`
  - Vem com funções de ZSH: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` e `slurp`
  - Vem com Neofetch e todas as suas dependências, Oh my Posh, Powerlevel10k e Znap
  - Vem com editores de texto no terminal: `micro`, `nano`, `neovim` e `vim`
  - Vem com `pandoc`
  - Vem com linguagens .NET, C, C++, Go, Java, Julia, Kotlin, Lua, Node.js (NVM) e `npm`, Python, R, Ruby e `gem`, Rust e Swift
  - Vem com frameworks Hugo e Jekyll
  - Vem com Homebrew, Kitty e MikTeX
  - Outras coisas: `ansi-art`, `chameolon`, `colorls`, `glow`, `htop`, `pip`, `pipes.sh`, `radian` e `rchitect`

  **Importante**

  Se Git Bash limita a transferência de um _container_ de uma imagem até a 1.5 GB, por isso, recomendamos que use o WSL com uma imagem mínima para transferir a imagem completa. 

  Devido aos *proxies* e das restrições da sua empresa, se você quiser ser capaz de instalar os pacotes via Node.js e Python, e clonar e atualizar os repositórios da empresa, recomendamos primeiramente a utilizar o WSL 1 e a exportar as configurações de _proxy_ no ficheiro `.bashrc` ou `.zshrc` antes. No WSL 2, sua empresa bloqueia as transferências, as instalações, os clones e os envios. Caso você queira usar o WSL 2, você deve usar [wsl-vpnkit](https://github.com/sakai135/wsl-vpnkit), mas esta utilizacão pode colocar você em risco. 
</details>

<details>
  <summary>English</summary>
  
  The minimal, medium and full images of Alpine Linux, Arch Linux, Ubuntu and Void Linux distributions are developed to meet the needs of the company's developers, engineers and programmers who can pull them from Docker, under the company's authorization, to export from Docker and import to WSL.

  The tags of the images – `minimal`, `middle` and `complete` – depend on what the contributor wants or needs. You can visit the tags comparison table and see which packages each tag has or does not have what you need. [Visit here](./tables.md)

  ### Which is your right choice?

  You can check without visiting the big table, but we will put a minimal list of applications, libraries and packages.

  #### `minimal`

  - It comes with Bash and ZSH
  - It comes with `curl`, `git`, `openssh`, `wget` and `xclip`
  - It comes with Bash and ZSH functions: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` and `tree`
  - It comes with zip and extract commands: `7zip`, `tar`, `unzip`, `zip` and `zstd`
  - It comes with ZSH functions: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` and `slurp`
  - It comes with Neofetch and all its dependencies, Powerlevel10k and Znap
  - It comes with text editors in the terminal: `micro` and `neovim`
  - It comes with `pandoc`
  - It comes with C, C++, Lua and Python with `pip` languages
  - Other stuff: `htop` and `pipes.sh`

  #### `middle`

  - It comes with Bash and ZSH
  - It comes with `curl`, `git`, `openssh`, `wget` and `xclip`
  - It comes with Bash and ZSH functions: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` and `tree`
  - It comes with compress and extract commands: `tar`, `unzip`, `zip` and `zstd`
  - It comes with ZSH functions: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` and `slurp`
  - It comes with Neofetch and all its dependencies, Oh My Posh, Powerlevel10k and Znap
  - It comes with text editors in the terminal: `micro` and `neovim`
  - It comes with `pandoc`
  - It comes with C, C++, Lua, Node.js (NVM) and Python with `pip` languages
  - It comes with Kitty
  - Other stuff: `chameolon`, `htop` and `pipes.sh`

  #### `complete`

  - It comes with Bash and ZSH
  - It comes with `curl`, `git`, `openssh`, `wget` and `xclip`
  - It comes with Bash and ZSH functions: `awk`. `bc`, `fzf`, `grep`, `qalc`, `sed` and `tree`
  - It comes with compress and extract commands: `tar`, `unzip`, `zip` and `zstd`
  - It comes with ZSH functions: `fd`, `grim`, `lesspie`, `lsd`, `peco`, `ripgrep` and `slurp`
  - It comes with Neofetch and all its dependencies, Oh my Posh, Powerlevel10k and Znap
  - It comes with text editors in the terminal: `micro`, `nano`, `neovim` and `vim`
  - It comes with `pandoc`
  - It comes with .NET, C, C++, Go, Java, Julia, Kotlin, Lua, Node.js (NVM) with `npm`, Python with `pip`, R, Ruby with `gem`, Rust and Swift languages
  - It comes with Hugo and Jekyll frameworks
  - It comes with Homebrew, Kitty and MikTeX
  - Other stuff: `ansi-art`, `chameolon`, `colorls`, `glow`, `htop`, `pipes.sh`, `radian` and `rchitect`

  **Important**

  If Git Bash limits the transfer of an image _container_ up to 1.5GB, then we recommend using WSL with a minimal image to transfer the full image.

  Due to your company's *proxies* and restrictions, if you want to be able to install packages via Node.js and Python, and clone, pull and push the company's repositories, we recommend using WSL 1 and exporting the _proxy_ settings in the file `.bashrc` or `.zshrc` before. In WSL 2, your company blocks downloads, installations, clones and uploads. If you want to use WSL 2, you have to use [wsl-vpnkit](https://github.com/sakai135/wsl-vpnkit), but this usage can put you at risk.
</details>
