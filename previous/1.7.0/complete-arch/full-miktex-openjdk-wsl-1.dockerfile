# Arch Linux

FROM archlinux/archlinux AS base-devel

LABEL description "Latest WSL 1 compatible version of Arch Linux image comes with Fish, git, htop, Micro, Neofetch, Oh My Posh, Oh My Zsh, Powerlevel10k, SSH and ZSH, minimal Python and complete NPM. It includes MikTeX and OpenJDK."
LABEL maintainer="Gustavo Costa <gusbemacbe@gmail.com>" 
LABEL vendor="Gustavo Costa" 
LABEL version="1.7.1"

# 🇬🇧 SYSTEM
# 🇵🇹 SISTEMA
# =============================

SHELL  ["/bin/bash", "-o", "pipefail", "-c"]

ENV TERM xterm-256color
RUN set -ex && ldconfig

COPY ./config/global/sh/colours.sh ./
RUN chmod au+x ./colours.sh

## 🇬🇧 Copy and setting pacman configuration filer
## 🇵🇹 A copiar e definir o ficheiro de a configuração do pacman
COPY ./config/arch/pacman.conf /etc/

## 🇬🇧 Firstly it needs to be synced and then “glibc” needs to be installed before generating the languages
## 🇵🇹 Primeiramente, precisa ser sincronizado e depois a biblioteca «glibc» precisa ser instalada antes de gerar os idiomas
RUN pacman -Sy --noconfirm
COPY ./config/downloads/aurs/glibc-2.33-5.zst ./
COPY ./config/downloads/aurs/lib32-glibc-2.33-5.zst ./
RUN pacman -U --noconfirm *.zst

## 🇬🇧 Setting the time zone and symlinking it with force, so it will not be stuck during the tzdata setting
## 🇵🇹 A definir o fuso horário e ligando-o simbolicamente com força, então não ficará preso durante a configuração de tzdata
ENV TZ America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## 🇬🇧 Installing my favourite languages and keyboard system
## 🇵🇹 A instalar os meus idiomas favoritos e o teclado do sistema
COPY ./config/global/languages/locale-minimal.gen /etc/locale.gen.orig
RUN sed -e 's/[[:space:]]*#.*//' -e '/^$/d' /etc/locale.gen.orig >/etc/locale.gen
RUN cat /etc/locale.gen && locale-gen

## Setting the 🇵🇹 language for the system
## A aplicar o idioma 🇵🇹 para o sistema
ENV LANG pt_PT.utf8
ENV LANGUAGE pt_PT:pt_BR:en  
ENV LC_ALL pt_PT.UTF-8

## 🇬🇧 Keyboard
## 🇵🇹 Teclado
COPY ./config/global/keyboards/kbd-apple-en-us-mac /etc/default/keyboard
RUN pacman -S --needed --noconfirm kbd xkeyboard-config

## 🇬🇧 Necessary missed dependencies for the system
## 🇵🇹 Dependências faltadas necessárias para o sistema
RUN pacman -S --needed --noconfirm base-devel
RUN pacman -S --needed --noconfirm gcc sudo

## 🇬🇧 Installing “openssh” to generate the SSH keys for the enterprise's GitLab account
## 🇵🇹 A instalar «openssh» para poder gerar as chaves de SSH para a conta do GitLab da empresa
RUN pacman -S --needed --noconfirm openssh

## 🇬🇧 Installing curl and wget to download the packages as Oh my Posh's and unzip to extract the packages as Oh my Posh's.
## 🇵🇹 A instalar curl e wget para poder transferir os pacotes como o de Oh my posh e unzip para extrair os pacotes, como o de Oh my Posh.
RUN pacman -S --needed --noconfirm curl git wget unzip

## 🇬🇧 Ah, pandoc and xclip are missed!
## pandoc: to convert the Markdown files to HTML or PDF, containing Pandoc and TeX variables.
## xclip: to able able to copy and paste from Micro Editor.
## 🇵🇹 Ah, faltam os pacotes pandoc e xclip!
## pandoc: para converter os ficheiros de Markdown para HTML ou PDF, contendo variáveis do Pandoc e do TeX.
## xclip: para capaz de copiar e colar do Micro Editor.
RUN pacman -S --needed --noconfirm pandoc pandoc-crossref xclip

## 🇬🇧 Our favourite tools that we like more and use more to show our computer up to someone. 
## 🇵🇹 Nossas ferramentas favoritas que gostamos mais e usamos mais para mostrar nosso computador a alguém.
RUN pacman -S --needed --noconfirm fish htop micro neofetch zsh

## 🇬🇧 Nodejs and NPM for installing the enterprise's Angular and Voxel libraries. 
## 🇵🇹 Nodejs e NPM para instalar as bibliotecas do Angular e do Voxel da empresa. 
RUN pacman -S --needed --noconfirm nodejs npm

## 🇬🇧 Python and PyPi for installing and using Jupyter and data science libraries as “iara-text”.
## 🇵🇹 Python e PyPi para instalar e utilizar Jupyter e as bibliotecas de ciência de dados como «iara-text».
RUN pacman -S --needed --noconfirm python python-pip

## 🇬🇧 Other tools that are not part part of Arch's official repositories
## chameleon: colour picker running from the terminal
## gitstatus: a Powerlevel10k plugin
## 🇵🇹 Outras ferramentas que não fazem parte dos repositórios oficiais do Arch
## chameleon: Coletor de cores executando do terminal
## gitstatus: um plugin de PowerLevel10K.
COPY ./config/downloads/aurs/chameleon-git.zst ./
COPY ./config/downloads/aurs/gitstatus.zst ./
RUN pacman -U --needed --noconfirm chameleon-git.zst
RUN pacman -U --needed --noconfirm gitstatus.zst

## 🇬🇧 Some tools for zsh need to be Neofetch for showing the images via Neofetch or the fancy features on Windows Terminal, Cmder or Kitty. 
## 🇵🇹 Faltam algumas ferramentas para Neofetch para mostrar as imagens via Neofetch ou as funções fancy no Windows Terminal, Cmder ou Kitty.
RUN pacman -S --needed --noconfirm chafa imagemagick libvterm w3m xdotool xorg-xdpyinfo xorg-xprop xorg-xrandr xorg-xwininfo xterm

## 🇬🇧 Since WSL and Windows Terminal do not support displaying the image with Neofetch, we can try to use Kitty
## 🇵🇹 Como o WSL e o Windows Terminal não suportam exibindo a imagem com Neofetch, podemos tentar usar Kitty
RUN pacman -S --needed --noconfirm kitty kitty-terminfo xorg-xauth

## 🇬🇧 Copying and installing Sixel tools. 
## 🇵🇹 A copiar e instalar as ferramentas de Sixel
COPY ./config/downloads/aurs/libsixel.zst ./
COPY ./config/downloads/aurs/lsix-git.zst ./
COPY ./config/downloads/aurs/sixel-tmux-git.zst ./
COPY ./config/downloads/aurs/vte3-git.zst ./
RUN pacman -U --need --noconfirm *.zst

## 🇬🇧 Installing LSDeluxe
## 🇵🇹 A instalar o LSDeluxe
ENV LSDELUXE_VERSION 0.20.1
RUN pacman -S --needed --noconfirm lsd

# 🇬🇧 Installing Oh My Posh, alternative for Oh my Zsh e ao Powerlevel10k.
# 🇵🇹 A instalar o Oh My Posh, alternativo ao Oh my Zsh e ao Powerlevel10k.
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh
RUN chmod +x /usr/local/bin/oh-my-posh

# 🇬🇧 To clean the caches, the cookies and temporary files for optimising Docker image container
# 🇵🇹 Para limpar o caches, os cookies e arquivos temporários de modo a optimizar o tamanho do container da imagem do Docker
RUN rm -rf /var/cache/pacman/pkg/* /tmp/* /var/tmp/*
RUN rm *.zst

# 🇬🇧 Changing the default shell. Goodbye, Bash! 💔
# 🇵🇹 A modificar o shell padrão. Adeus, Bash! 💔
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A mudar o shell padrão para ZSH. Adeus, Bash! 💔.\n" && chsh -s /usr/bin/zsh

# 🇬🇧 Entering the new shell
# 🇵🇹 A entrar no novo shell
RUN zsh

# 🇬🇧 USER
# 🇵🇹 UTILIZADOR
# =============================

# 🇬🇧 Creating the user. 
# 🇵🇹 A criar o utilizador. 
RUN echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user

COPY ./config/global/sudoers /etc/sudoers
RUN chmod 0440 /etc/sudoers.d/user
RUN chmod 0440 /etc/sudoers

ENV UUSER benegus
ENV GROUP www-data
ARG USER_FOLDER=/home/$UUSER

RUN useradd -m $UUSER && groupadd $GROUP
RUN usermod -aG root $UUSER
RUN usermod -aG users $UUSER
RUN usermod -aG wheel $UUSER
RUN usermod -aG $GROUP $UUSER
RUN usermod -g root $UUSER
RUN usermod -u 1001 $UUSER
RUN usermod $UUSER -p "$(openssl passwd -1 piltover-and-zaun)"
RUN groups $UUSER

RUN rm ./colours.sh

USER $UUSER
WORKDIR $USER_FOLDER

# 🇬🇧 Creating the needed folders. 
# 🇵🇹 A criar as pastas necessárias.
COPY ./config/global/sh/colours.sh ./
RUN mkdir -pv $USER_FOLDER/.{aspnet,config/neofetch/{ascii,images,styles},dotnet,fzf,jupyter,kite,local/{bin,share/{apps,icons}},node_modules/cache,p10k/themes,poetry,poshthemes,r,ssh,yarn/global}
RUN mkdir -pv $USER_FOLDER/{Documentos,Git,GitHub,GitLab,Imagens,Transferências,Videos,Workspaces}

# 🇬🇧 Installing FZF - executable only (required for “zsh-interactive-cd”)
# 🇵🇹 A instalar o FZF - somente executável (obrigatório para «zsh-interactive-cd»)
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A transferir e instalar o FZF.\n" && git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
RUN ~/.fzf/install

# 🇬🇧 Downlading Oh My Posh themes
# 🇵🇹 A transferir os temas de Oh My Posh
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O ~/.poshthemes/themes.zip
RUN unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
RUN chmod u+rw ~/.poshthemes/*.json
RUN rm ~/.poshthemes/themes.zip

# 🇬🇧 Downloading and installing Oh my ZSH
# 🇵🇹 A transferir e  instalar o Oh My ZSH
RUN wget -qO- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh || true

ARG ZSH_CUSTOM=$USER_FOLDER/.oh-my-zsh/custom

# 🇬🇧 Installing Oh My ZSH plugins and themes, and Powerlevel10k
# 🇵🇹 A instalar os plugins e os temas de Oh My ZSH, e Powerlevel10k
ENV ZSH_PLUGINS $ZSH_CUSTOM/plugins
ENV ZSH_THEMES $ZSH_CUSTOM/themes

# 🇬🇧 Downloading and installing the ZSH and Oh my ZSH add-ons, and installing PowerLevel10K.
# 🇵🇹 A transferir e instalar o os complementos de ZSH e Oh My ZSH, e a instalar o Powerlevel10k.
RUN git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_PLUGINS/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-completions.git $ZSH_PLUGINS/zsh-completions
RUN git clone https://github.com/zsh-users/zsh-history-substring-search  $ZSH_PLUGINS/zsh-history-substring-search
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_PLUGINS/zsh-syntax-highlighting
RUN git clone https://github.com/romkatv/powerlevel10k.git $ZSH_THEMES/powerlevel10k

# 🇬🇧 To copy the PowerLevel10K theme and ZSH configuration files.
# 🇵🇹 A copiar o tema de Powerlevel10k e os ficheiros de configuração de ZSH.
COPY --chown=$UUSER:$GROUP ./config/global/zsh/themes/*.zsh $USER_FOLDER/.p10k/themes/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/.zshrc $USER_FOLDER/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/alias/aliases.zsh $ZSH_CUSTOM

# 🇬🇧 Copying the ANSI and ASII art files to Neofetch folders
# 🇵🇹 A copiar os ficheiros de arte de ANSI e ASCII à pasta de Neofetch
COPY ./config/global/ansi/with-neofetch/itau*.txt $USER_FOLDER/.config/neofetch/ascii/
COPY ./config/global/images/* $USER_FOLDER/.config/neofetch/images/

# 🇬🇧 NPM, PNPM AND YARN
# 🇵🇹 NPM, PNPM E YARN
# =============================
RUN npm config set prefix ~/.node_modules && \
    npm config set cache ~/.node_modules/cache
RUN export PATH="~/.node_modules/bin:$PATH"

# Yarn
RUN npm i -g yarn
# PNPM
RUN curl -fsSL https://get.pnpm.io/install.sh | sh -

# 🇬🇧 Copying Yarn configuration file.
# 🇵🇹 A copiar o ficheiro de configuração de Yarn.
COPY --chown=$UUSER:$GROUP ./config/global/node/package.json $USER_FOLDER/.config/yarn/global/

# PYTHON
# =============================
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

# 🇬🇧 OTHERS
# 🇵🇹 OUTROS
# =============================

# 🇬🇧 Returing as root
# 🇵🇹 A retornar como root.
USER root

# Installing MikTeX
# A instalar o MikTeX
COPY ./config/downloads/aurs/miktex.zst ./
RUN pacman -U --noconfirm miktex.zst && rm miktex.zst

### 🇬🇧 Installing OpenJDK 8, 11 e 17
### 🇵🇹 A instalar OpenJDK das versnoes 8, 11 e 17
RUN pacman -S --noconfirm jdk8-openjdk jdk11-openjdk jdk-openjdk

# 🇬🇧 To clean the caches to optimise the Docker container size
# 🇵🇹 Para limpar os caches para optimizar o tamanho do container do Docker
RUN rm -rf /tmp/*
RUN pacman -Sccc --noconfirm
RUN npm cache clean --force && npm cache verify

USER $UUSER
WORKDIR $USER_FOLDER

# 🇬🇧 Exporting MikTex environment variables:
# 🇵🇹 A exportar as variáveis do ambiente de MikTeX:
ENV MIKTEX_USERCONFIG $USER_FOLDER/.miktex/texmfs/config
ENV MIKTEX_USERDATA $USER_FOLDER/.miktex/texmfs/data
ENV MIKTEX_USERINSTALL $USER_FOLDER/.miktex/texmfs/install

# 🇬🇧 Refreshing MikTeX environment variables for the shell:
# 🇵🇹 A atualizar variáveis do ambiente do MikTeX ao shell:
ENV PATH="/opt/miktex/bin:$PATH"

# 🇬🇧 Updating the settings and installing MikTeX libraries:
# 🇵🇹 A atualizar as configurações e a instalar as bibliotecas do MikTeX:
RUN miktexsetup finish --user-link-target-directory=$USER_FOLDER/.miktex/bin && \
    mpm --update-db && \
    initexmf --set-config-value [MPM]AutoInstall=1 && \
    initexmf --update-fndb && \
    initexmf --mklinks
RUN mpm --verbose --package-level=basic --upgrade
RUN mpm --verbose --install abnt && \
    mpm --verbose --install abntex2 && \
    mpm --verbose --install accanthis && \
    mpm --verbose --install auxhook && \
    mpm --verbose --install babel-portuges && \
    mpm --verbose --install biblatex-abnt && \
    mpm --verbose --install bigintcalc && \
    mpm --verbose --install bitset && \
    mpm --verbose --install bookmark && \
    mpm --verbose --install datetime2-portuges && \
    mpm --verbose --install etexcmds && \
    mpm --verbose --install geometry && \
    mpm --verbose --install gettitlestring && \
    mpm --verbose --install glossaries-portuges && \
    mpm --verbose --install glyphlist && \
    mpm --verbose --install hycolor && \
    mpm --verbose --install infwarerr && \
    mpm --verbose --install intcalc && \
    mpm --verbose --install kvdefinekeys && \
    mpm --verbose --install kvoptions && \
    mpm --verbose --install kvsetkeys && \
    mpm --verbose --install letltxmacro && \
    mpm --verbose --install lm-math && \
    mpm --verbose --install ltxcmds && \
    mpm --verbose --install microtype && \
    mpm --verbose --install parskip && \
    mpm --verbose --install pdfescape && \
    mpm --verbose --install refcount && \
    mpm --verbose --install rerunfilecheck && \
    mpm --verbose --install stringenc && \
    mpm --verbose --install unicode-math && \
    mpm --verbose --install uniquecounter && \
    mpm --verbose --install upquote && \
    mpm --verbose --install xurl
RUN initexmf --mkmaps && cat ~/.miktex/texmfs/config/miktex/config/miktex.ini

RUN source ./colours.sh && echo -en "$(piltover_2 'Fim da construção e pronto para a publicação.')\n"

# RUN xauth add gustavo-endeavour/unix:0  MIT-MAGIC-COOKIE-1  4be697fc158742b3befce30e8616ec60 && \
#     xauth add X370GT7/unix:0  MIT-MAGIC-COOKIE-1  33848675efe5fdffcf4bfb792ae5cbc0 && \
#     xauth list

# EXPOSE 8887
# CMD kitty

CMD ["zsh"]