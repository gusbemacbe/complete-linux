# Ubuntu 21.10 Impish

FROM ubuntu:rolling AS base

LABEL description "Latest and complete version of Ubuntu 21.10 image comes with Git, Micro, Neofetch, Oh My Zsh, Powerlevel10k, SSH and ZSH. It comes with Hugo, Jekyll, MikTeX, .NET Core, OpenJDK 8, 11 e 17, R and Ruby."
LABEL maintainer="Gustavo Costa <gusbemacbe@gmail.com>" 
LABEL vendor="Gustavo Costa" 
LABEL version="1.7.1"

# 🇬🇧 SYSTEM
# 🇵🇹 SISTEMA
# =============================

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV TERM xterm-256color
RUN set -ex && ldconfig

ARG DEBIAN_FRONTEND=noninteractive

COPY ./config/global/sh/colours.sh ./
RUN chmod au+x ./colours.sh

## 🇬🇧 Copy and setting pacman configuration filer
## 🇵🇹 A copiar e definir o ficheiro de a configuração do pacman
COPY ./config/arch/pacman.conf /etc/

## 🇬🇧 Firstly it needs to be synced and then “glibc” (“locales”) needs to be installed before generating the languages
## 🇵🇹 Primeiramente, precisa ser sincronizado e depois a biblioteca «glibc» (“locales”) precisa ser instalada antes de gerar os idiomas
RUN apt update && apt upgrade -y && apt install -y locales locales-all

## 🇬🇧 Setting the time zone and symlinking it with force, so it will not be stuck during the tzdata setting
## 🇵🇹 Definindo o fuso horário e ligando-o simbolicamente com força, então não ficará preso durante a configuração de tzdata
ENV TZ America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## 🇬🇧 Installing my favourite languages and keyboard system
## 🇵🇹 A instalar os meus idiomas favoritos e o teclado do sistema
RUN source ./colours.sh && echo -en "$(piltover_2 4:) A instalar os meus idiomas favoritos e o teclado do sistema.\n"
COPY ./config/global/languages/locale-minimal.gen /etc/locale.gen.orig
RUN sed -e 's/[[:space:]]*#.*//' -e '/^$/d' /etc/locale.gen.orig >/etc/locale.gen
RUN cat /etc/locale.gen && locale-gen

## 🇬🇧 Setting the 🇵🇹 language for the system
## 🇵🇹 A aplicar o idioma 🇵🇹 para o sistema
ENV LANG pt_PT.utf8
ENV LANGUAGE pt_PT:pt_BR:en  
ENV LC_ALL pt_PT.UTF-8

## 🇬🇧 Keyboard
## 🇵🇹 Teclado
COPY ./config/global/keyboards/kbd-apple-en-us-mac /etc/default/keyboard
RUN apt install -y kbd

## 🇬🇧 Necessary missed dependencies for the system
## 🇵🇹 Dependências faltadas necessárias para o sistema
RUN apt install -y build-essential dirmngr software-properties-common
RUN apt install -y gcc gfortran sudo

## 🇬🇧 Installing “openssh” to generate the SSH keys for the enterprise's GitLab account
## 🇵🇹 A instalar «openssh» para poder gerar as chaves de SSH para a conta do GitLab da empresa
RUN apt install -y openssh-client openssh-server

## 🇬🇧 Installing curl and wget to download the packages as Oh my Posh's and unzip to extract the packages as Oh my Posh's.
## 🇵🇹 A instalar curl e wget para poder transferir os pacotes como o de Oh my posh e unzip para extrair os pacotes, como o de Oh my Posh.
RUN apt install -y ca-certificates curl git p7zip p7zip-full wget unzip

## 🇬🇧 Ah, pandoc and xclip are missed!
## pandoc: to convert the Markdown files to HTML or PDF, containing Pandoc and TeX variables.
## xclip: to able able to copy and paste from Micro Editor.
## 🇵🇹 Ah, faltam os pacotes pandoc e xclip!
## pandoc: para converter os ficheiros de Markdown para HTML ou PDF, contendo variáveis do Pandoc e do TeX.
## xclip: para capaz de copiar e colar do Micro Editor.
RUN apt install -y pandoc xclip

## 🇬🇧 Our favourite tools that we like more and use more to show our computer up to someone. 
## 🇵🇹 Nossas ferramentas favoritas que gostamos mais e usamos mais para mostrar nosso computador a alguém.
RUN apt install -y htop micro neofetch zsh

## 🇬🇧 Nodejs and NPM for installing the enterprise's Angular and Voxel libraries. 
## 🇵🇹 Nodejs e NPM para instalar as bibliotecas do Angular e do Voxel da empresa. 
RUN apt install -y nodejs npm

## 🇬🇧 Python and PyPi for installing and using Jupyter and data science libraries as “iara-text”.
## 🇵🇹 Python e PyPi para instalar e utilizar Jupyter e as bibliotecas de ciência de dados como «iara-text».
RUN apt install -y python3 python3-pip

## 🇬🇧 Other tools that are not part part of Arch's official repositories
## chameleon: colour picker running from the terminal
## gitstatus: a Powerlevel10k plugin
## 🇵🇹 Outras ferramentas que não fazem parte dos repositórios oficiais do Arch
## chameleon: Coletor de cores executando do terminal
## gitstatus: um plugin de PowerLevel10K.
RUN pip3 install moving-chameleon
RUN mkdir -p /usr/share/gitstatus
COPY ./config/downloads/source/gitstatus.7z /usr/share/gitstatus/
RUN cd /usr/share/gitstatus && 7z x -t7z gitstatus.7z
RUN rm /usr/share/gitstatus/gitstatus.7z

## 🇬🇧 Some tools for zsh need to be Neofetch for showing the images via Neofetch or the fancy features on Windows Terminal, Cmder or Kitty. 
## 🇵🇹 Faltam algumas ferramentas para Neofetch para mostrar as imagens via Neofetch ou as funções fancy no Windows Terminal, Cmder ou Kitty.
RUN apt install -y chafa imagemagick libvterm0 w3m w3m-img xdotool x11-utils x11-xserver-utils xterm

## 🇬🇧 Since WSL and Windows Terminal do not support displaying the image with Neofetch, we can try to use Kitty
## 🇵🇹 Como o WSL e o Windows Terminal não suportam exibindo a imagem com Neofetch, podemos tentar usar Kitty
RUN apt install -y kitty kitty-terminfo xauth

## 🇬🇧 Installing Sixel tools. 
## 🇵🇹 Instalar as ferramentas de Sixel
RUN apt install -y libsixel-bin
COPY ./config/downloads/source/lsix.7z /usr/bin/
RUN cd /usr/bin/ && 7z x -t7z lsix.7z && chmod au+x /usr/bin/lsix && rm /usr/bin/lsix.7z

## 🇬🇧 Installing LSDeluxe
## 🇵🇹 A instalar o LSDeluxe
ENV LSDELUXE_VERSION 0.21.0
RUN wget -nv -O lsdeluxe.deb https://github.com/Peltoche/lsd/releases/download/${LSDELUXE_VERSION}/lsd_${LSDELUXE_VERSION}_amd64.deb
RUN dpkg -i lsdeluxe.deb && rm lsdeluxe.deb

## 🇬🇧 Installing Oh My Posh, alternative for Oh my Zsh e ao Powerlevel10k.
## 🇵🇹 A instalar o Oh My Posh, alternativo ao Oh my Zsh e ao Powerlevel10k.
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh
RUN chmod +x /usr/local/bin/oh-my-posh

## 🇬🇧 To clean the caches, the cookies and temporary files for optimising Docker image container
## 🇵🇹 Para limpar o caches, os cookies e arquivos temporários de modo a optimizar o tamanho do container da imagem do Docker
RUN apt-get -y clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## 🇬🇧 Changing the default shell. Goodbye, Bash! 💔
## 🇵🇹 A modificar o shell padrão. Adeus, Bash! 💔
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A mudar o shell padrão para ZSH. Adeus, Bash! 💔.\n" && chsh -s /usr/bin/zsh

## 🇬🇧 Entering the new shell
## 🇵🇹 A entrar no novo shell
RUN zsh

# 🇬🇧 USER
# 🇵🇹 UTILIZADOR
# =============================

## 🇬🇧 Creating the user. 
## 🇵🇹 A criar o utilizador. 
RUN echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user
COPY ./config/global/sudoers /etc/sudoers
RUN chmod 0440 /etc/sudoers.d/user
RUN chmod 0440 /etc/sudoers

ENV UUSER benegus
ENV GROUP www-data
ARG USER_FOLDER=/home/$UUSER

RUN useradd -m $UUSER
RUN usermod -aG root $UUSER
RUN usermod -aG users $UUSER
RUN addgroup wheel
RUN usermod -aG wheel $UUSER
RUN usermod -aG $GROUP $UUSER
RUN usermod -g root $UUSER
RUN usermod -u 1001 $UUSER
RUN usermod $UUSER -p "$(openssl passwd -1 piltover-and-zaun)"
RUN groups $UUSER

RUN rm ./colours.sh

USER $UUSER
WORKDIR $USER_FOLDER

## 🇬🇧 Creating the needed folders. 
## 🇵🇹 A criar as pastas necessárias.
COPY ./config/global/sh/colours.sh ./
RUN mkdir -pv $USER_FOLDER/.{aspnet,config/neofetch/{ascii,images,styles},dotnet,jupyter,kite,local/{bin,share/{apps,icons}},fzf,miktex/texmfs/{config,data,install},p10k/themes,poetry,poshthemes,ssh}
RUN mkdir -pv $USER_FOLDER/{Documentos,Git,GitHub,GitLab,Imagens,Transferências,Videos,Workspaces}

## 🇬🇧 Installing FZF - executable only (required for “zsh-interactive-cd”)
## 🇵🇹 A instalar o FZF - somente executável (obrigatório para «zsh-interactive-cd»)
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A transferir e instalar o FZF.\n" && git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
RUN ~/.fzf/install

## 🇬🇧 Downlading Oh My Posh themes
## 🇵🇹 A transferir os temas de Oh My Posh
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O ~/.poshthemes/themes.zip
RUN unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
RUN chmod u+rw ~/.poshthemes/*.json
RUN rm ~/.poshthemes/themes.zip

## 🇬🇧 Downloading and installing Oh my ZSH
## 🇵🇹 A transferir e  instalar o Oh My ZSH
RUN wget -qO- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh || true

ARG ZSH_CUSTOM=$USER_FOLDER/.oh-my-zsh/custom

## 🇬🇧 Installing Oh My ZSH plugins and themes, and Powerlevel10k
## 🇵🇹 A instalar os plugins e os temas de Oh My ZSH, e Powerlevel10k
ENV ZSH_PLUGINS $ZSH_CUSTOM/plugins
ENV ZSH_THEMES $ZSH_CUSTOM/themes

## 🇬🇧 Downloading and installing the ZSH and Oh my ZSH add-ons, and installing PowerLevel10K.
## 🇵🇹 A transferir e instalar o os complementos de ZSH e Oh My ZSH, e a instalar o Powerlevel10k.
RUN git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_PLUGINS/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-completions.git $ZSH_PLUGINS/zsh-completions
RUN git clone https://github.com/zsh-users/zsh-history-substring-search  $ZSH_PLUGINS/zsh-history-substring-search
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_PLUGINS/zsh-syntax-highlighting
RUN git clone https://github.com/romkatv/powerlevel10k.git $ZSH_THEMES/powerlevel10k

## 🇬🇧 To copy the PowerLevel10K theme and ZSH configuration files.
## 🇵🇹 A copiar o tema de Powerlevel10k e os ficheiros de configuração de ZSH.
COPY --chown=$UUSER:$GROUP ./config/global/zsh/themes/*.zsh $USER_FOLDER/.p10k/themes/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/.zshrc $USER_FOLDER/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/alias/aliases.zsh $ZSH_CUSTOM

## 🇬🇧 Copying the ANSI and ASII art files to Neofetch folders
## 🇵🇹 A copiar os ficheiros de arte de ANSI e ASCII à pasta de Neofetch
COPY ./config/global/ansi/with-neofetch/itau*.txt $USER_FOLDER/.config/neofetch/ascii/
COPY ./config/global/images/* $USER_FOLDER/.config/neofetch/images/

## 🇬🇧 NPM, PNPM AND YARN
## 🇵🇹 NPM, PNPM E YARN
## =============================
RUN npm config set prefix ~/.node_modules && \
    npm config set cache ~/.node_modules/cache
RUN export PATH="~/.node_modules/bin:$PATH"

### Yarn
RUN npm i -g yarn
### PNPM
RUN curl -fsSL https://get.pnpm.io/install.sh | sh -

### 🇬🇧 Copying Yarn configuration file.
### 🇵🇹 A copiar o ficheiro de configuração de Yarn.
COPY --chown=$UUSER:$GROUP ./config/global/node/package.json $USER_FOLDER/.config/yarn/global/

## PYTHON
## =============================

### Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -

## 🇬🇧 OTHERS
## 🇵🇹 OUTROS
## =============================

### 🇬🇧 Returing as root
### 🇵🇹 A retornar como root.
USER root

RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A começar a construir MikTeX:\n"

COPY ./config/downloads/source/mkitex.tar.gz ./
RUN tar -xvf mkitex.tar.gz && mv miktex-22.1 miktex

RUN apt update
RUN apt install -y bison
RUN apt install -y cmake
RUN apt install -y dpkg-dev
RUN apt install -y file
RUN apt install -y flex
RUN apt install -y g++
RUN apt install -y gettext
RUN apt install -y gosu
RUN apt install -y libboost-locale-dev
RUN apt install -y libbz2-dev
RUN apt install -y libcairo2-dev
RUN apt install -y libcurl4-openssl-dev
RUN apt install -y libfribidi-dev
RUN apt install -y libgd-dev
RUN apt install -y libgmp-dev
RUN apt install -y libgraphite2-dev
RUN apt install -y libharfbuzz-dev
RUN apt install -y libhunspell-dev
RUN apt install -y libicu-dev
RUN apt install -y liblog4cxx-dev
RUN apt install -y libmpfr-dev
RUN apt install -y libmspack-dev
RUN apt install -y libpopt-dev
RUN apt install -y libpotrace-dev
RUN apt install -y libssl-dev
RUN apt install -y liburiparser-dev
RUN apt install -y libzzip-dev
RUN apt install -y make
RUN apt install -y qtbase5-dev
RUN apt install -y qtdeclarative5-dev
RUN apt install -y qtscript5-dev
RUN apt install -y qttools5-dev
RUN apt install -y xsltproc
RUN apt install -y xz-utils

RUN cd miktex && [ -d build ] || mkdir build
RUN cd miktex/build && cmake \
    -DMIKTEX_PACKAGE_REVISION=1-1 \
    -DMIKTEX_LINUX_DIST=ubuntu \
    -DMIKTEX_LINUX_DIST_VERSION=21.10 \
    -DMIKTEX_LINUX_DIST_CODE_NAME=impish \
    -DCMAKE_INSTALL_PREFIX=/opt/miktex \
    -DUSE_SYSTEM_HARFBUZZ=FALSE \
    -DUSE_SYSTEM_HARFBUZZ_ICU=FALSE \
    -DUSE_SYSTEM_POPPLER=FALSE \
    -DUSE_SYSTEM_POPPLER_QT5=FALSE \
    -DWITH_UI_QT=TRUE \
    .. \
    && make && make test && make install

RUN cp -r /opt/miktex/share/applications/miktex-console.desktop /usr/share/applications/miktex-console.desktop && \
    sed -i 's/^Exec=miktex-console$/Exec=\/opt\/miktex\/bin\/miktex-console/' /usr/share/applications/miktex-console.desktop && \
    cp -R /opt/miktex/share/applications/icons /usr/share/ && \
    cp -r /opt/miktex/man /usr/share/man

### 🇬🇧 Installing OpenJDK 8, 11 e 17
### 🇵🇹 A instalar OpenJDK das versnoes 8, 11 e 17
RUN apt install -y openjdk-8-jdk openjdk-11-jdk openjdk-17-jdk

### 🇬🇧 Installing C# and .NET 5 e 6
### 🇵🇹 A instalar C# e .NET 5 e 6
COPY ./config/global/sh/dotnet-install.sh ./
RUN chmod au+x dotnet-install.sh && mkdir -p $USER_FOLDER/.dotnet
RUN ./dotnet-install.sh --install-dir $USER_FOLDER/.dotnet -c Current -version latest && \
    ./dotnet-install.sh --install-dir $USER_FOLDER/.dotnet -c 5.0 -version latest && \
    export PATH="$USER_FOLDER/.dotnet:$PATH" && dotnet --list-sdks && dotnet --version

### 🇬🇧 Installing Hugo, Jekyll, R and Ruby
### 🇵🇹 A instalar Hugo, Jekyll, R e Ruby
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
    add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/" && \
    apt update -qq && apt install -y hugo r-base rbenv

### 🇬🇧 To clean the caches to optimise the Docker container size
### 🇵🇹 Para limpar os caches para optimizar o tamanho do container do Docker
RUN rm -rf /tmp/*
RUN apt -y clean && apt -y autoclean && apt -y autoremove
RUN npm cache clean --force && npm cache verify && rm -rf miktex mkitex.tar.gz

USER $UUSER
WORKDIR $USER_FOLDER

### 🇬🇧 Exporting MikTex environment variables:
### 🇵🇹 A exportar as variáveis do ambiente de MikTeX:
ENV MIKTEX_USERCONFIG $USER_FOLDER/.miktex/texmfs/config
ENV MIKTEX_USERDATA $USER_FOLDER/.miktex/texmfs/data
ENV MIKTEX_USERINSTALL $USER_FOLDER/.miktex/texmfs/install

### 🇬🇧 Refreshing MikTeX, Python and Ruby environment variables for the shell:
### 🇵🇹 A atualizar variáveis do ambiente do MikTeX, Python and Ruby ao shell:
ENV PATH $USER_FOLDER/.local/bin:$USER_FOLDER/.rbenv/shims:$USER_FOLDER/.local/share/gem/ruby/3.0.0/bin:/opt/miktex/bin:$PATH

# RUN rbenv install --list 

# RUN echo "eval \"$(rbenv init -)\"" >> ~/.bashrc && \
#     echo "eval \"$(rbenv init -)\"" >> ~/.zshrc && \
#     rbenv install rbx-4.15 && rbenv global rbx-4.15 && rbenv version && ruby --version

#     pip3 install -U --no-cache-dir lineedit && \
#     pip3 install -U --no-cache-dir rchitect && \
#     pip3 install -U --no-cache-dir radian && \
#     rbenv local --unset && rbenv local 3.1.0 && \
#     rbenv shell --unset && rbenv shell 3.1.0 && \
#     curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash && \
#     gem install bundler jekyll && \
#     which {hugo,jekyll,R,radian,ruby}

### 🇬🇧 Updating the settings and installing MikTeX libraries:
### 🇵🇹 A atualizar as configurações e a instalar as bibliotecas do MikTeX:
RUN miktexsetup finish --user-link-target-directory=${USER_FOLDER}/.miktex/bin && \
    mpm --update-db && \
    initexmf --set-config-value [MPM]AutoInstall=1 && \
    initexmf --update-fndb && \
    initexmf --mklinks
RUN mpm --verbose --package-level=basic --upgrade
RUN mpm --verbose --install abnt && \
    mpm --verbose --install abntex2 && \
    mpm --verbose --install accanthis && \
    mpm --verbose --install auxhook && \
    mpm --verbose --install babel-portuges && \
    mpm --verbose --install biblatex-abnt && \
    mpm --verbose --install bigintcalc && \
    mpm --verbose --install bitset && \
    mpm --verbose --install bookmark && \
    mpm --verbose --install datetime2-portuges && \
    mpm --verbose --install etexcmds && \
    mpm --verbose --install geometry && \
    mpm --verbose --install gettitlestring && \
    mpm --verbose --install glossaries-portuges && \
    mpm --verbose --install glyphlist && \
    mpm --verbose --install hycolor && \
    mpm --verbose --install infwarerr && \
    mpm --verbose --install intcalc && \
    mpm --verbose --install kvdefinekeys && \
    mpm --verbose --install kvoptions && \
    mpm --verbose --install kvsetkeys && \
    mpm --verbose --install letltxmacro && \
    mpm --verbose --install lm-math && \
    mpm --verbose --install ltxcmds && \
    mpm --verbose --install microtype && \
    mpm --verbose --install parskip && \
    mpm --verbose --install pdfescape && \
    mpm --verbose --install refcount && \
    mpm --verbose --install rerunfilecheck && \
    mpm --verbose --install stringenc && \
    mpm --verbose --install unicode-math && \
    mpm --verbose --install uniquecounter && \
    mpm --verbose --install upquote && \
    mpm --verbose --install xurl
RUN initexmf --mkmaps && cat ~/.miktex/texmfs/config/miktex/config/miktex.ini

RUN source ./colours.sh && echo -en "$(piltover_2 35:) Fim da construção e pronto para a publicação.\n"

CMD ["zsh"]
