# Ubuntu 22.04 Jammy

FROM ubuntu:devel AS base

LABEL description "Minimal version of Ubuntu 22.04 image comes with Git, Micro, Neofetch, Oh My Zsh, Powerlevel10k, SSH and ZSH"
LABEL maintainer="Gustavo Costa <gusbemacbe@gmail.com>" 
LABEL vendor="Gustavo Costa" 
LABEL version="1.6.3"

# 🇬🇧 SYSTEM
# 🇵🇹 SISTEMA
# =============================

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV TERM xterm-256color
RUN set -ex && ldconfig

ARG DEBIAN_FRONTEND=noninteractive

COPY ./config/sh/colours.sh ./
RUN chmod au+x ./colours.sh

# 🇬🇧 Setting pacman configuration
# 🇵🇹 A definir a configuração do pacman
COPY ./config/arch/pacman.conf /etc/

# 🇬🇧 Firstly it needs to be synced and then “glibc” (“locales”) needs to be installed before generating the languages
# 🇵🇹 Primeiramente, precisa ser sincronizado e depois a biblioteca «glibc» (“locales”) precisa ser instalada antes de gerar os idiomas
RUN apt update && apt upgrade -y && apt install -y locales locales-all

# 🇬🇧 Setting the time zone and symlinking it with force, so it will not be stuck during the tzdata setting
# 🇵🇹 Definindo o fuso horário e ligando-o simbolicamente com força, então não ficará preso durante a configuração de tzdata
ENV TZ America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## 🇬🇧 Installing my favourite languages and keyboard system
## 🇵🇹 A instalar os meus idiomas favoritos e o teclado do sistema
COPY ./config/languages/locale-minimal.gen /etc/locale.gen.orig
RUN sed -e 's/[[:space:]]*#.*//' -e '/^$/d' /etc/locale.gen.orig >/etc/locale.gen
RUN cat /etc/locale.gen && locale-gen

## 🇬🇧 Setting the 🇵🇹 language for the system
## 🇵🇹 A aplicar o idioma 🇵🇹 para o sistema
ENV LANG pt_PT.utf8
ENV LANGUAGE pt_PT:pt_BR:en  
ENV LC_ALL pt_PT.UTF-8

# 🇬🇧 Keyboard
# 🇵🇹 Teclado
COPY ./config/global/keyboards/kbd-apple-en-us-mac /etc/default/keyboard
RUN apt install -y kbd

# 🇬🇧 Necessary missed dependencies for the system
# 🇵🇹 Dependências faltadas necessárias para o sistema
RUN apt install -y build-essential
RUN apt install -y gcc sudo

## 🇬🇧 Installing “openssh” to generate the SSH keys for the enterprise's GitLab account
## 🇵🇹 A instalar «openssh» para poder gerar as chaves de SSH para a conta do GitLab da empresa
RUN apt install -y openssh-client openssh-server

## 🇬🇧 Installing curl and wget to download the packages as Oh my Posh's and unzip to extract the packages as Oh my Posh's.
## 🇵🇹 A instalar curl e wget para poder transferir os pacotes como o de Oh my posh e unzip para extrair os pacotes, como o de Oh my Posh.
RUN apt install -y ca-certificates curl git p7zip p7zip-full wget unzip

## 🇬🇧 Ah, pandoc and xclip are missed!
## pandoc: to convert the Markdown files to HTML or PDF, containing Pandoc and TeX variables.
## xclip: to able able to copy and paste from Micro Editor.
## 🇵🇹 Ah, faltam os pacotes pandoc e xclip!
## pandoc: para converter os ficheiros de Markdown para HTML ou PDF, contendo variáveis do Pandoc e do TeX.
## xclip: para capaz de copiar e colar do Micro Editor.
RUN apt install -y pandoc xclip

## 🇬🇧 Our favourite tools that we like more and use more to show our computer up to someone. 
## 🇵🇹 Nossas ferramentas favoritas que gostamos mais e usamos mais para mostrar nosso computador a alguém.
RUN apt install -y htop micro neofetch zsh

## 🇬🇧 Nodejs and NPM for installing the enterprise's Angular and Voxel libraries. 
## 🇵🇹 Nodejs e NPM para instalar as bibliotecas do Angular e do Voxel da empresa. 
RUN apt install -y nodejs npm

## 🇬🇧 Python and PyPi for installing and using Jupyter and data science libraries as “iara-text”.
## 🇵🇹 Python e PyPi para instalar e utilizar Jupyter e as bibliotecas de ciência de dados como «iara-text».
RUN apt install -y python3 python3-pip

## 🇬🇧 Other tools that are not part part of Arch's official repositories
## chameleon: colour picker running from the terminal
## gitstatus: a Powerlevel10k plugin
## 🇵🇹 Outras ferramentas que não fazem parte dos repositórios oficiais do Arch
## chameleon: Coletor de cores executando do terminal
## gitstatus: um plugin de PowerLevel10K.
RUN pip3 install moving-chameleon
RUN mkdir -p /usr/share/gitstatus
COPY ./config/downloads/source/gitstatus.7z /usr/share/gitstatus/
RUN cd /usr/share/gitstatus && 7z x -t7z gitstatus.7z
RUN rm /usr/share/gitstatus/gitstatus.7z

## 🇬🇧 Some tools for zsh need to be Neofetch for showing the images via Neofetch or the fancy features on Windows Terminal, Cmder or Kitty. 
## 🇵🇹 Faltam algumas ferramentas para Neofetch para mostrar as imagens via Neofetch ou as funções fancy no Windows Terminal, Cmder ou Kitty.
RUN apt install -y chafa imagemagick libvterm0 w3m w3m-img xdotool x11-utils x11-xserver-utils xterm

## 🇬🇧 Since WSL and Windows Terminal do not support displaying the image with Neofetch, we can try to use Kitty
## 🇵🇹 Como o WSL e o Windows Terminal não suportam exibindo a imagem com Neofetch, podemos tentar usar Kitty
RUN apt install -y kitty kitty-terminfo xauth

## 🇬🇧 Installing Sixel tools. 
## 🇵🇹 Instalar as ferramentas de Sixel
RUN apt install -y libsixel-bin
COPY ./config/downloads/source/lsix.7z /usr/bin/
RUN cd /usr/bin/ && 7z x -t7z lsix.7z && chmod au+x /usr/bin/lsix && rm /usr/bin/lsix.7z

## 🇬🇧 Installing LSDeluxe
## 🇵🇹 A instalar o LSDeluxe
ENV LSDELUXE_VERSION 0.21.0
RUN wget -nv -O lsdeluxe.deb https://github.com/Peltoche/lsd/releases/download/${LSDELUXE_VERSION}/lsd_${LSDELUXE_VERSION}_amd64.deb
RUN dpkg -i lsdeluxe.deb && rm lsdeluxe.deb

# 🇬🇧 Installing Oh My Posh
# 🇵🇹 A instalar o Oh My Posh
RUN source ./colours.sh && echo -en "$(piltover_2 14:) A instalar o Oh My Posh, alternativo ao Oh my Zsh e ao Powerlevel10k.\n"
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64 -O /usr/local/bin/oh-my-posh
RUN chmod +x /usr/local/bin/oh-my-posh

# 🇬🇧 To clean the cache and temporary files
# 🇵🇹 Para limpar o cache e arquivos temporários
RUN source ./colours.sh && echo -en "$(piltover_2 15:) A limpar os $(italico caches) e os $(italico cookies) de modo a optimizar o tamanho do $(italico container) da imagem do Docker.\n"
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

## 🇬🇧 Changing the default shell. Goodbye, Bash! 💔
## 🇵🇹 A modificar o shell padrão. Adeus, Bash! 💔
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A mudar o shell padrão para ZSH. Adeus, Bash! 💔.\n" && chsh -s /usr/bin/zsh

# 🇬🇧 Entering the new shell
# 🇵🇹 A entrar no novo shell
RUN zsh

# 🇬🇧 USER
# 🇵🇹 UTILIZADOR
# =============================

## 🇬🇧 Creating the user. 
## 🇵🇹 A criar o utilizador. 
RUN echo "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user
COPY ./config/sudoers /etc/sudoers
RUN chmod 0440 /etc/sudoers.d/user
RUN chmod 0440 /etc/sudoers

ENV UUSER benegus
ENV GROUP www-data
ARG USER_FOLDER=/home/$UUSER

RUN useradd -m $UUSER
RUN usermod -aG root $UUSER
RUN usermod -aG users $UUSER
RUN addgroup wheel
RUN usermod -aG wheel $UUSER
RUN usermod -aG $GROUP $UUSER
RUN usermod -g root $UUSER
RUN usermod -u 1001 $UUSER
RUN usermod $UUSER -p "$(openssl passwd -1 piltover-and-zaun)"
RUN groups $UUSER

RUN rm ./colours.sh

USER $UUSER
WORKDIR $USER_FOLDER

## 🇬🇧 Creating the needed folders. 
## 🇵🇹 A criar as pastas necessárias.
COPY ./config/global/sh/colours.sh ./
RUN mkdir -pv $USER_FOLDER/.{aspnet,config/neofetch/{ascii,images,styles},dotnet,jupyter,kite,local/{bin,share/{apps,icons}},fzf,miktex/texmfs/{config,data,install},p10k/themes,poetry,poshthemes,ssh}
RUN mkdir -pv $USER_FOLDER/{Documentos,Git,GitHub,GitLab,Imagens,Transferências,Videos,Workspaces}

## 🇬🇧 Installing FZF - executable only (required for “zsh-interactive-cd”)
## 🇵🇹 A instalar o FZF - somente executável (obrigatório para «zsh-interactive-cd»)
RUN source ./colours.sh && echo -en "$(piltover_2 'Descrição:') A transferir e instalar o FZF.\n" && git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
RUN ~/.fzf/install

# 🇬🇧 Downlading Oh My Posh themes
# 🇵🇹 A transferir os temas de Oh My Posh
RUN wget https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip -O ~/.poshthemes/themes.zip
RUN unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
RUN chmod u+rw ~/.poshthemes/*.json
RUN rm ~/.poshthemes/themes.zip

## 🇬🇧 Downloading and installing Oh my ZSH
## 🇵🇹 A transferir e  instalar o Oh My ZSH
RUN wget -qO- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh || true

ARG ZSH_CUSTOM=$USER_FOLDER/.oh-my-zsh/custom

# 🇬🇧 Installing Oh My ZSH plugins and themes, and Powerlevel10k
# 🇵🇹 A instalar os plugins e os temas de Oh My ZSH, e Powerlevel10k
ENV ZSH_PLUGINS $ZSH_CUSTOM/plugins
ENV ZSH_THEMES $ZSH_CUSTOM/themes

## 🇬🇧 Downloading and installing the ZSH and Oh my ZSH add-ons, and installing PowerLevel10K.
## 🇵🇹 A transferir e instalar o os complementos de ZSH e Oh My ZSH, e a instalar o Powerlevel10k.
RUN git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_PLUGINS/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-completions.git $ZSH_PLUGINS/zsh-completions
RUN git clone https://github.com/zsh-users/zsh-history-substring-search  $ZSH_PLUGINS/zsh-history-substring-search
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_PLUGINS/zsh-syntax-highlighting
RUN git clone https://github.com/romkatv/powerlevel10k.git $ZSH_THEMES/powerlevel10k

## 🇬🇧 To copy the PowerLevel10K theme and ZSH configuration files.
## 🇵🇹 A copiar o tema de Powerlevel10k e os ficheiros de configuração de ZSH.
COPY --chown=$UUSER:$GROUP ./config/global/zsh/themes/*.zsh $USER_FOLDER/.p10k/themes/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/.zshrc $USER_FOLDER/
COPY --chown=$UUSER:$GROUP ./config/global/zsh/alias/aliases.zsh $ZSH_CUSTOM

## 🇬🇧 Copying the ANSI and ASII art files to Neofetch folders
## 🇵🇹 A copiar os ficheiros de arte de ANSI e ASCII à pasta de Neofetch
COPY ./config/global/ansi/with-neofetch/itau*.txt $USER_FOLDER/.config/neofetch/ascii/
COPY ./config/global/images/* $USER_FOLDER/.config/neofetch/images/

## 🇬🇧 NPM, PNPM AND YARN
## 🇵🇹 NPM, PNPM E YARN
## =============================
RUN npm config set prefix ~/.node_modules && \
    npm config set cache ~/.node_modules/cache
RUN export PATH="~/.node_modules/bin:$PATH"

### Yarn
RUN npm i -g yarn
### PNPM
RUN curl -fsSL https://get.pnpm.io/install.sh | sh -

### 🇬🇧 Copying Yarn configuration file.
### 🇵🇹 A copiar o ficheiro de configuração de Yarn.
COPY --chown=$UUSER:$GROUP ./config/global/node/package.json $USER_FOLDER/.config/yarn/global/

## PYTHON
## =============================

### Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -

## 🇬🇧 OTHERS
## 🇵🇹 OUTROS
## =============================

### 🇬🇧 Returing as root
### 🇵🇹 A retornar como root.
USER root

### 🇬🇧 To clean the caches to optimise the Docker container size
### 🇵🇹 Para limpar os caches para optimizar o tamanho do container do Docker
RUN rm -rf /tmp/*
RUN apt -y clean && apt -y autoclean && apt -y autoremove

RUN npm cache clean --force && npm cache verify

USER $UUSER
WORKDIR $USER_FOLDER

RUN source ./colours.sh && echo -en "$(piltover_2 35:) Fim da construção e pronto para a publicação.\n"

CMD ["zsh"]
