#!/bin/bash

source $(which colours)

alpine="apk"
archlinux="pacman"
fedora="dnf"
gentoo="emerge"
mageia="urpmi"
opensuse="zypper"
solus="eopkg"
ubuntu="apt"
voidlinux="xbps-install"

if [[ -x "$(command -v apk)" ]];              then echo "$(piltover 'Gestor de pacotes:') Adélie e Alpine"
  elif [[ -x "$(command -v apt-get)" ]];      then echo "$(piltover 'Gestor de pacotes:') Debian e derivados"
  elif [[ -x "$(command -v dnf)" ]];          then echo "$(piltover 'Gestor de pacotes:') CentOS, Fedora, Mageia, OpenMandriva e RedHat"
  elif [[ -x "$(command -v emerge)" ]];       then echo "$(piltover 'Gestor de pacotes:') Funtoo e Gentoo"
  elif [[ -x "$(command -v eopkg)" ]];        then echo "$(piltover 'Gestor de pacotes:') Solus"
  elif [[ -x "$(command -v pacman)" ]];       then echo "$(piltover 'Gestor de pacotes:') Arch Linux e derivados"
  elif [[ -x "$(command -v urpmi)" ]];        then echo "$(piltover 'Gestor de pacotes:') Mageia"
  elif [[ -x "$(command -v xbps-install)" ]]; then echo "$(piltover 'Gestor de pacotes:') Void Linux"
  elif [[ -x "$(command -v zypper)" ]];       then echo "$(piltover 'Gestor de pacotes:') openSUSE"
  else echo "Gestor de pacotes desconhecido";
fi

get_package_manager_update()
{
  if   [[ -x "$(command -v apk)" ]];          then apk update         # apk update
  elif [[ -x "$(command -v apt-get)" ]];      then apt update         # apt update
  elif [[ -x "$(command -v dnf)" ]];          then dnf check-update   # dnf check-update
  elif [[ -x "$(command -v emerge)" ]];       then emerge --sync      # emerge --sync
  elif [[ -x "$(command -v eopkg)" ]];        then eopkg upgrade      # eopkg upgrade
  elif [[ -x "$(command -v pacman)" ]];       then pacman -Sy         # pacman -Sy
  elif [[ -x "$(command -v urpmi)" ]];        then urpmi.update       # urpmi.update
  elif [[ -x "$(command -v xbps-install)" ]]; then xbps-install -S    # xbps-install -S
  elif [[ -x "$(command -v zypper)" ]];       then zypper up          # zypper up
  else        
    echo "Gestor do pacote desconhecido" &>/dev/null/;
  fi
}

get_package_manager_upgrade()
{
  if   [[ -x "$(command -v apk)" ]];          then apk upgrade --available   # apk upgrade --available
  elif [[ -x "$(command -v apt-get)" ]];      then apt-get upgrade -y        # apt upgrade -y
  elif [[ -x "$(command -v dnf)" ]];          then dnf upgrade -y            # dnf upgrade -y
  elif [[ -x "$(command -v emerge)" ]];       then emerge -puvD world        # emerge -puvD world
  elif [[ -x "$(command -v eopkg)" ]];        then eopkg upgrade             # eopkg upgrade 
  elif [[ -x "$(command -v pacman)" ]];       then pacman -Syyu --noconfirm  # pacman -Syyu
  elif [[ -x "$(command -v urpmi)" ]];        then urpmi --auto-update       # urpmi --auto-update
  elif [[ -x "$(command -v xbps-install)" ]]; then xbps-install -u           # xbps-install -u
  elif [[ -x "$(command -v zypper)" ]];       then zypper dup                # zypper dup
  else        
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi
}

get_package_manager_clean()
{
  if   [[ -x "$(command -v apk)" ]]; then 
    apk cache clean
    rm -rvf /tmp/* /var/tmp/*
  elif [[ -x "$(command -v apt-get)" ]]; then 
    apt-get -y clean autoclean autoremove
    rm -rvf /var/lib/apt/lists/* /tmp/* /var/tmp/*
  elif [[ -x "$(command -v dnf)" ]]; then 
    dnf clean all
    rm -rvf /tmp/* /var/tmp/*
  elif [[ -x "$(command -v emerge)" ]]; then 
    eclean-dist
    eclean-pkg
    rm -rvf /tmp/* /var/tmp/*
  elif [[ -x "$(command -v eopkg)" ]]; then 
    eopkg dc
    rm -rvf /tmp/* /var/tmp/*
  elif [[ -x "$(command -v pacman)" ]]; then 
    pacman -Sccc --noconfirm
    rm -rf /var/cache/pacman/pkg/* /tmp/* /var/tmp/* *.zst
  elif [[ -x "$(command -v urpmi)" ]]; then 
    urpme −−auto−orphans
    uprmi −−clean
  elif [[ -x "$(command -v xbps-install)" ]]; then 
    xbps-remove -R -Oo -y
  elif [[ -x "$(command -v zypper)" ]]; then 
    zypper rm -u -y
    zypper cc -a
  else        
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi
}

get_package_manager_install()
{
  if [[ -x "$(command -v $1)" ]]; then
    "$1 $2 $3"
  else        
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi
}

echo "$(piltover '# 🇬🇧 Description:') Creating the needed folders."
echo "$(piltover '# 🇵🇹 Descrição:')   A criar as pastas necessárias."
install -d -m 0755 -o benegus -g www-data -v /home/benegus/.{aspnet,dotnet,jupyter,kite,miktex/texmfs/{config,data,install},p10k/themes,poetry,poshthemes,ssh}
install -d -m 0755 -o benegus -g www-data -v /home/benegus/{Documentos,Git,GitHub,GitLab,Imagens,Transferências,Videos,Workspaces}

echo "$(piltover '🇬🇧 Returing as root.')"
echo "$(piltover '🇵🇹 A retornar como root.')"

echo "$(piltover '# 🇬🇧 Description:') Firstly it needs to be synced and then “glibc” (“locales”) needs to be installed before generating the languages:"
echo "$(piltover '# 🇵🇹 Descrição:')   Primeiramente, precisa ser sincronizado e depois a biblioteca «glibc» (“locales”) precisa ser instalada antes de gerar os idiomas:"

get_package_manager_update
get_package_manager_upgrade

# get_package_manager_install "$alpine"        'add --no-cache'          'musl-locales'
# get_package_manager_install "$archlinux"     '-S --noconfirm --needed' 'glibc'
# get_package_manager_install "$fedora"        'install -y'              'glibc'
# get_package_manager_install "$gentoo"        '-av'                     'sys-libs/glibc'
# get_package_manager_install "$mageia"        ''                        'glibc'
# get_package_manager_install "$opensuse"      'in'                      'glibc'
# get_package_manager_install "$solus"         'install'                 'glibc'
# get_package_manager_install "$ubuntu"        'install -y'              'locales locales-all'
# get_package_manager_install "$voidlinuxl"    '-Su -y'                  'glibc'

echo "$(piltover '# 🇬🇧 Description:') Setting the time zone and symlinking it with force, so it will not be stuck during the tzdata setting:"
echo "$(piltover '# 🇵🇹 Descrição:')   Definindo o fuso horário e ligando-o simbolicamente com força, então não ficará preso durante a configuração de tzdata:"
TZ="America/Sao_Paulo"
export TZ="America/Sao_Paulo"
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

echo "$(piltover '# 🇬🇧 Description:') Installing my favourite system languages and my favourite keyboard:"
echo "$(piltover '# 🇵🇹 Descrição:') A instalar os meus idiomas favoritos e o meu teclado favorito do sistema:"
cp -rv /etc/languages/locale-minimal.gen /etc/locale.gen.orig
sed -e 's/[[:space:]]*#.*//' -e '/^$/d' /etc/locale.gen.orig > /etc/locale.gen
cat /etc/locale.gen && locale-gen

echo "$(piltover '# 🇬🇧 Description:') Setting the 🇵🇹 language for the system:"
echo "$(piltover '# 🇵🇹 Descrição:') A aplicar o idioma 🇵🇹 para o sistema:"
LANG="pt_PT.utf8"
LANGUAGE="pt_PT:pt_BR:en"
LC_ALL="pt_PT.UTF-8"
export LANG="pt_PT.utf8"
export LANGUAGE="pt_PT:pt_BR:en"
export LC_ALL="pt_PT.UTF-8"

echo "$(piltover '# 🇬🇧 Description:') Keyboard:"
echo "$(piltover '# 🇵🇹 Descrição:')   Teclado:"
mkdir -pv /etc/default
cp -rv /etc/keyboards/kbd-apple-en-us-mac /etc/default/keyboard

get_package_manager_install "$alpine"        "add --no-cache"          "kbd"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "kbd"
get_package_manager_install "$fedora"        "install -y"              "kbd"
get_package_manager_install "$gentoo"        "-av"                     "kbd"
get_package_manager_install "$mageia"        ""                        "kbd"
get_package_manager_install "$opensuse"      "in"                      "kbd"
get_package_manager_install "$solus"         "install"                 "kbd"
get_package_manager_install "$ubuntu"        "install -y"              "kbd"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "kbd"

echo "$(piltover '# 🇬🇧 Description:') Necessary missed dependencies for the system"
echo "$(piltover '# 🇵🇹 Descrição:')   Dependências faltadas necessárias para o sistema"

get_package_manager_install "$alpine"        "add --no-cache"          "automake gcc g++ gfortran lua5.1 make"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "automake gcc gcc-fortran lua make"
get_package_manager_install "$fedora"        "install -y"              "automake gcc gcc-c++ gcc-gfortran lua make"
get_package_manager_install "$gentoo"        "-av"                     "dev-lang/cfortran dev-lang/lua sys-devel/automake sys-devel/gcc sys-devel/make"
get_package_manager_install "$mageia"        ""                        "automake gcc gcc-c++ gcc-gfortran lua make"
get_package_manager_install "$opensuse"      "in"                      "automake gcc gcc-c++ gcc-gfortran lua54 make"
get_package_manager_install "$solus"         "install"                 "automake gcc g++ gfortran lua-devel make"
get_package_manager_install "$ubuntu"        "install -y"              "automake gcc g++ gfortran lua5.4 make"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "automake gcc gcc-fortran lua54 make"

echo "$(piltover '# 🇬🇧 Description:') SSH to generate the SSH keys for the enterprise's GitLab account"
echo "$(piltover '# 🇵🇹 Descrição:')   SSH para gerar as chaves de SSH para a conta do GitLab da empresa"

get_package_manager_install "$alpine"        "add --no-cache"          "openssh openssh-client openssh-server"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "openssh"
get_package_manager_install "$fedora"        "install -y"              "openssh openssh-server"
get_package_manager_install "$gentoo"        "-av"                     "net-misc/openssh"
get_package_manager_install "$mageia"        ""                        "openssh openssh-server"
get_package_manager_install "$opensuse"      "in"                      "openssh openssh-server"
get_package_manager_install "$solus"         "install"                 "openssh openssh-server"
get_package_manager_install "$ubuntu"        "install -y"              "openssh-client openssh-server"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "openssh"

echo "$(piltover '# 🇬🇧 Description:') Installing curl and wget to download the packages as Oh my Posh's and unzip to extract the packages as Oh my Posh's."
echo "$(piltover '# 🇵🇹 Descrição:')   A instalar curl e wget para poder transferir os pacotes como o de Oh my posh e unzip para extrair os pacotes, como o de Oh my Posh."

get_package_manager_install "$alpine"        "add --no-cache"          "ca-certificates curl git p7zip wget unzip zstd"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "ca-certificates curl git p7zip wget unzip zstd"
get_package_manager_install "$fedora"        "install -y"              "ca-certificates curl git p7zip wget unzip zstd"
get_package_manager_install "$gentoo"        "-av"                     "acct-group/git app-arch/p7zip app-arch/unzip app-arch/zstd app-misc/ca-certificates net-misc/curl net-misc/wget"
get_package_manager_install "$mageia"        ""                        "rootcerts curl git wget p7zip unzip zstd"
get_package_manager_install "$opensuse"      "in"                      "7zip ca-certificates curl git wget unzip zstd"
get_package_manager_install "$solus"         "install"                 "ca-certificates curl git p7zip wget unzip zstd"
get_package_manager_install "$ubuntu"        "install -y"              "ca-certificates curl git p7zip wget unzip zstd"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "ca-certificates curl git p7zip wget unzip zstd"

echo "$(piltover '# 🇬🇧 Description:') Ah, pandoc and xclip are missed!"
## pandoc: to convert the Markdown files to HTML or PDF, containing Pandoc and TeX variables.
## xclip: to able able to copy and paste from Micro Editor.
echo "$(piltover '# 🇵🇹 Descrição:')   Ah, faltam os pacotes pandoc e xclip!"
## pandoc: para converter os ficheiros de Markdown para HTML ou PDF, contendo variáveis do Pandoc e do TeX.
## xclip: para capaz de copiar e colar do Micro Editor.

get_package_manager_install "$alpine"        "add --no-cache"          "pandoc xclip"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "pandoc pandoc-crossref xclip"
get_package_manager_install "$fedora"        "install -y"              "pandoc xclip"
get_package_manager_install "$gentoo"        "-av"                     "app-text/pandoc 11-misc/xclip"
get_package_manager_install "$mageia"        ""                        "xclip"
get_package_manager_install "$opensuse"      "in"                      "pandoc xclip"
get_package_manager_install "$solus"         "install"                 "pandoc pandoc-crossref xclip"
get_package_manager_install "$ubuntu"        "install -y"              "pandoc xclip"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "pandoc xclip"

# Mageia não tem pandoc na sua biblioteca, por isto, devemos construir pandoc. 

echo "$(piltover '# 🇬🇧 Description:') Our favourite tools that we like more and use more to show our computer up to someone."
echo "$(piltover '# 🇵🇹 Descrição:')   Nossas ferramentas favoritas que gostamos mais e usamos mais para mostrar nosso computador a alguém."

get_package_manager_install "$alpine"        "add --no-cache"          "htop micro neofetch neovim zsh"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "htop micro neofetch neovim zsh"
get_package_manager_install "$fedora"        "install -y"              "htop micro neofetch neovim zsh"
get_package_manager_install "$gentoo"        "-av"                     "sys-process/htop app-editors/micro app-editors/neovim app-misc/neofetch app-shells/zsh"
get_package_manager_install "$mageia"        ""                        "htop micro neofetch neovim zsh"
get_package_manager_install "$opensuse"      "in"                      "htop micro-editor neofetch neovim zsh"
get_package_manager_install "$solus"         "install"                 "htop micro neofetch neovim zsh"
get_package_manager_install "$ubuntu"        "install -y"              "htop micro neofetch neovim zsh"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "htop micro neofetch neovim nvimpager zsh"

# Void Linux é o único que tem nvimpager

## nvimpager depende de neovim

if  [[ -x "$(command -v apk)" || -x "$(command -v apt-get)" || -x "$(command -v dnf)" || -x "$(command -v emerge)" || -x "$(command -v eopkg)" || -x "$(command -v urpmi)" || -x "$(command -v zypper)" ]]
then 
  echo "A instalar NVimpager...";
  mkdir -vp /home/benegus/Transferências/nvimpager
  tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/nvimpager.xbps -C /home/benegus/Transferências/nvimpager/

  cp -rv /home/benegus/Transferências/nvimpager/usr/bin/* /usr/bin/
  cp -rv /home/benegus/Transferências/nvimpager/usr/share/* /usr/share/
else        
  echo "Gestor do pacote desconhecido" &>/dev/null;
fi

echo "$(piltover '# 🇬🇧 Description:') Some tools for zsh need to be Neofetch for showing the images via Neofetch or the fancy features on Windows Terminal, Cmder or Kitty. "
echo "$(piltover '# 🇵🇹 Descrição:')   Faltam algumas ferramentas para Neofetch para mostrar as imagens via Neofetch ou as funções $(italico fancy) no Windows Terminal, Cmder ou Kitty."

get_package_manager_install "$alpine"        "add --no-cache"          "iceauth sessreg xcmsdb xdotool xdpyinfo xdriinfo xev xfd xfontsel xgamma xhost xkbcomp xkill xorg-appress xmessage xmodmap xprop xrandr xrdb xrefresh xset xvinfo xwininfo"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "luit xdotool xdriinfo xorg-appres xorg-iceauth xorg-xcmsdb xorg-xgamma xorg-sessreg xorg-xdpyinfo xorg-xev xorg-xfd xorg-xfontsel xorg-xhost xorg-xkbcomp xorg-xkill xorg-xlsatoms xorg-xlsclients xorg-xlsfonts xorg-xmessage xorg-xmodmap xorg-xprop xorg-xrandr xorg-xrdb xorg-xrefresh xorg-xset xorg-xvidtune xorg-xvinfo xorg-xwininfo"
get_package_manager_install "$fedora"        "install -y"              "appres editres glx-utils iceauth luit xdotool xdpyinfo xev xfontsel xgamma xhost xkbcomp xkill xlsatoms xlsclients xlsfonts xmessage xmodmap xprop xrandr xrdb xrefresh xvidtune xvinfo xwininfo"
get_package_manager_install "$gentoo"        "-av"                     "x11-misc/xdotool x11-apps/appres x11-apps/appres/editres x11-apps/iceauth x11-apps/listres x11-apps/luit x11-apps/sessreg x11-apps/viewres x11-apps/xcmsdb x11-apps/xdpyinfo x11-apps/xdriinfo x11-apps/xev x11-apps/xfd x11-apps/xfontsel x11-apps/xhost x11-apps/xkbcomp x11-apps/xkill x11-apps/xlsatoms x11-apps/xlsclients x11-apps/xlsfonts x11-apps/xmessage x11-apps/xmodmap x11-apps/xprop x11-apps/xrandr x11-apps/xrdb x11-apps/xrefresh x11-apps/xset x11-apps/xvinfo x11-apps/xwininfo"
get_package_manager_install "$mageia"        ""                        "appres editres iceauth listres luit sessreg viewres xdotool xdpyinfo xdriinfo xev xfd xfontsel xgamma xhost xkbcomp xkill xlsatoms xlsclients xlsfonts xmessage xmodmap xorg-x11 xprop xrandr xrdb xrefresh xset xvidtune xvinfo xwininfo"
get_package_manager_install "$opensuse"      "in"                      "appres editres iceauth listres luit sessreg viewres xcmsdb xdotool xdpyinfo xdriinfo xev xfd xfontsel xgamma xhost xkbcomp xkill xlsatoms xlsclients xlsfonts xmessage xmodmap xorg-x11 xprop xrandr xrdb xrefresh xset xvidtune xvinfo xwininfo"
get_package_manager_install "$solus"         "install"                 "luit xdotool xdpyinfo xev xgamma xhost xkbcomp xkill xmodmap xprop xrandr xrdb xset xwininfo"
get_package_manager_install "$ubuntu"        "install -y"              "xdotool x11-utils x11-xserver-utils"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "appres editres iceauth listres sessreg viewres xcmsdb xdotool xdpyinfo xdriinfo xev xfd xfontsel xgamma xhost xkbcomp xkill xlsatoms xlsclients xlsfonts xmessage xmodmap xprop xrandr xrdb xrefresh xset xvidtune xvinfo xwininfo"

# Alpine: não tem xlsatoms, xlsclients, xlsfonts e xvidtune.
# Arch Linux: editres, listres e viewres vêm de AUR.
# Fedora: não tem xcmsdb e xfd
# Solus: não tem iceauth, sessreg, x11-utils, xcmsdb, xfd, xfontsel, xlsatoms, xlsclients, xlsfonts, xmessage e xvidtune. 
# Void Linux: não tem luit. 

echo "$(piltover '# 🇬🇧 Description:') Some missed useful tools used in the existing Bash, Fish and ZSH files functions"
echo "$(piltover '# 🇵🇹 Descrição:')   Algumas perdidas ferramentas úteis utilizadas nas funções existentes nos ficheiros de Bash, Fish e ZSH"
get_package_manager_install "$alpine"        "add --no-cache"          "coreutils fzf grim iproute2 iptables iputils kmod libxkbcommon mtr net-tools ripgrep slurp sysstat traceroute util-linux-misc xkbcli zbar"
get_package_manager_install "$archlinux"     "-S --noconfirm --needed" "coreutils fzf grc grim iproute2 iptables iputils kmod libxkbcommon mtr net-tools ripgrep ripgrep-all slurp sysstat traceroute util-linux wdiff zbar"
get_package_manager_install "$fedora"        "install -y"              "coreutils fzf grc grim iproute iptables iputils kmod libxkbcommon mtr net-tools ripgrep slurp sysstat traceroute util-linux wdiff zbar"
get_package_manager_install "$gentoo"        "-av"                     "app-admin/sysstat app-misc/grc app-shells/fzf app-text/wdiff gui-apps/grim gui-apps/slurp media-gfx/zbar net-analyzer/mtr net-analyzer/traceroute net-firewall/iptables net-misc/iputils sys-apps/coreutils sys-apps/iproute2 sys-apps/kmod sys-apps/net-tools sys-apps/ripgrep sys-apps/ripgrep-all sys-apps/util-linux x11-libs/libxkbcommon"
get_package_manager_install "$mageia"        ""                        "coreutils iproute2 iptables iputils kmod ripgrep libxkbcommon-utils mtr net-tools sysstat traceroute util-linux wdiff zbar"
get_package_manager_install "$opensuse"      "in"                      "coreutils fzf grc grim iproute2 iptables iputils libxkbcommon-tools mtr net-tools-deprecated ripgrep slurp sysstat traceroute util-linux wdiff zbar"
get_package_manager_install "$solus"         "install"                 "coreutils fzf grim inetutils iproute2 iptables iputils kmod libxkbcommon mtr ripgrep slurp sysstat util-linux zbar"
get_package_manager_install "$ubuntu"        "install -y"              "coreutils fdisk fzf grc grim iproute2 iptables iputils-arping iputils-ping iputils-tracepath kmod libxkbcommon-tools mtr net-tools ripgrep slurp sysstat traceroute wdiff zbar-tools"
get_package_manager_install "$voidlinuxl"    "-Su -y"                  "coreutils fzf grc grim iproute2 iptables iputils kmod libxkbcommon libxkbcommon-tools mtr ripgrep slurp sysstat traceroute util-linux zbar"

# Arch e Gentoo são os únicos que têm ripgrep-all. 
# Mageia é o único que não tem grim e slurp. Devemos construir os pacotes. 
# Alpine, Mageia e Solus não têm grc. 
# Void Linux não tem net-tools.
# Alpine, Solus e Void Linux não tem wdiff.
# Mageia não tem fzf. 

## grim depende das bibliotecas cairo, libjpeg-turbo e wayland, e slurp de libxkbcommon

mkdir -vp /home/benegus/Transferências/{fzf,grc,grim,lesspipe,lsd,net-tools,peco,ripgrep-all,slurp,wdiff}

if  [[ -x "$(command -v apk)" || -x "$(command -v apt-get)" || -x "$(command -v dnf)" || -x "$(command -v emerge)" || -x "$(command -v eopkg)" || -x "$(command -v urpmi)" || -x "$(command -v zypper)" ]]
then 
  echo "A instalar ripgrep...";
  tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/ripgrep-all.zst -C /home/benegus/Transferências/ripgrep-all/
  cp -rv /home/benegus/Transferências/ripgrep-all/usr/bin/* /usr/bin/

  # fzf, grim, slurp for Mageia
  if [[ -x "$(command -v urpmi)" ]]
  then
    echo "A instalar fzf, grim e slurp...";
    tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/grim.zst -C /home/benegus/Transferências/grim/
    tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/slurp.zst -C /home/benegus/Transferências/slurp/
    tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/fzf.zst -C /home/benegus/Transferências/fzf/

    cp -rv /home/benegus/Transferências/grim/usr/bin/* /usr/bin/
    cp -rv /home/benegus/Transferências/grim/usr/share/* /usr/share/
    
    cp -rv /home/benegus/Transferências/slurp/usr/bin/* /usr/bin/
    cp -rv /home/benegus/Transferências/slurp/usr/share/* /usr/share/

    cp -rv /home/benegus/Transferências/fzf/usr/bin/* /usr/bin/
    cp -rv /home/benegus/Transferências/fzf/usr/share/* /usr/share/
  else
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi

  # grc for Alpine, Mageia and Solus
  if  [[ -x "$(command -v apk)" || -x "$(command -v eopkg)" || -x "$(command -v urpmi)" ]]
  then 
      echo "A instalar grc...";
      tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/grc.zst -C /home/benegus/Transferências/grc/
      cp -rv /home/benegus/Transferências/grc/etc/* /etc/
      cp -rv /home/benegus/Transferências/grc/usr/bin/* /usr/bin/
      cp -rv /home/benegus/Transferências/grc/usr/share/* /usr/share/
  else
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi

  # net-tools for Void Linux
  if  [[ -x "$(command -v xbps-install)" ]]
  then 
      echo "A instalar net-tools...";
      tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/net-tools.zst -C /home/benegus/Transferências/net-tools/
      cp -rv /home/benegus/Transferências/net-tools/usr/bin/* /usr/bin/
      cp -rv /home/benegus/Transferências/net-tools/usr/share/* /usr/share/
  else
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi

  # wdiff for Alpine, Solus and Void Linux
  if  [[ -x "$(command -v apk)" || -x "$(command -v eopkg)" || -x "$(command -v xbps-install)" ]]
  then 
      echo "A instalar wdiff...";
      tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/wdiff.zst -C /home/benegus/Transferências/wdiff/
      cp -rv /home/benegus/Transferências/wdiff/usr/bin/* /usr/bin/
      cp -rv /home/benegus/Transferências/wdiff/usr/share/* /usr/share/
  else
    echo "Gestor do pacote desconhecido" &>/dev/null;
  fi

else        
  echo "Gestor do pacote desconhecido" &>/dev/null;
fi

# Peco
echo "A instalar Peco...";
tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/peco.zst -C /home/benegus/Transferências/peco/
cp -rv /home/benegus/Transferências/peco/usr/bin/* /usr/bin/
cp -rv /home/benegus/Transferências/peco/usr/share/* /usr/share/

# Lesspipe
echo "A instalar Lesspipe...";
tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/lesspipe.zst -C /home/benegus/Transferências/lesspipe/
cp -rv /home/benegus/Transferências/lesspipe/etc/* /etc/
cp -rv /home/benegus/Transferências/lesspipe/usr/bin/* /usr/bin/
cp -rv /home/benegus/Transferências/lesspipe/usr/share/* /usr/share/

echo "$(piltover '# 🇬🇧 Description:') Installing LSDeluxe"
echo "$(piltover '# 🇵🇹 Descrição:')   A instalar o LSDeluxe"
LSDELUXE_VERSION=0.21.0
export LSDELUXE_VERSION=0.21.0

if  [[ -x "$(command -v apk)" ]]
then
  tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/lsd-musl.zst -C /home/benegus/Transferências/lsd/
else
  tar --use-compress-program=unzstd -xvf /home/benegus/Transferências/lsd.zst -C /home/benegus/Transferências/lsd/
fi

cp -rv /home/benegus/Transferências/lsd/usr/bin/lsd /usr/bin/
cp -rv /home/benegus/Transferências/lsd/usr/share /usr/

rm -rf /home/benegus/Transferências/*

echo "$(piltover '# 🇬🇧 Description:') To clean the caches, the cookies and temporary files for optimising Docker image container"
echo "$(piltover '# 🇵🇹 Descrição:')   Para limpar o caches, os cookies e arquivos temporários de modo a optimizar o tamanho do container da imagem do Docker"
get_package_manager_clean

echo "$(piltover '# 🇬🇧 Description:') Changing the default shell. Goodbye, Bash! 💔"
echo "$(piltover '# 🇵🇹 Descrição:')   A modificar o shell padrão. Adeus, Bash! 💔"
# chsh -s /usr/bin/zsh

echo "$(piltover '# 🇬🇧 Description:') 🇬🇧 Entering the new shell"
echo "$(piltover '# 🇵🇹 Descrição:')   🇵🇹 A entrar no novo shell"
# zsh