FROM mageia:cauldron

ARG BASE
ARG DEBIAN_FRONTEND
ARG DESCRIPTION
ARG MAINTAINER
ARG TAG
ARG VENDOR
ARG VERSION

ENV GROUP www-data
ENV TERM xterm-256color
ENV UUSER benegus

# INSTALAÇÕES EXTRAS
## Middle
ENV CHAMELEON no
ENV FANCY_NEOFETCH_FEATURES no
ENV GITSTATUS no
ENV NODEJS no
ENV OHMYPOSH no
ENV OHMYPOSHTHEMES no
ENV PIP no

## Middle + MiKTeX
ENV MIKTEX no

## Middle + MiKTeX + OpenJDK
ENV OPENJDK no

LABEL distro "Mageia Linux $BASE"
LABEL description "$DESCRIPTION"
LABEL tag "$TAG"
LABEL version "$VERSION"
LABEL maintainer "$MAINTAINER"
LABEL vendor "$VENDOR" 

# 🇬🇧 SYSTEM
# 🇵🇹 SISTEMA
# =============================

SHELL  ["/bin/bash", "-o", "pipefail", "-c"]
RUN set -ex && ldconfig

# 🇬🇧 Firstly it needs to be synced to install glibc, openssl and sudo
# 🇵🇹 Primeiramente, precisa ser sincronizado para instalar glibc, openssl e sudo 
RUN urpmi --auto-update
RUN urpmi glibc glibc-i18ndata
RUN urpmi bc binutils coreutils findutils grep openssl pciutils sed shadow-utils sudo tar usbutils util-linux which

# 🇬🇧 USER
# 🇵🇹 UTILIZADOR
# =============================

## 🇬🇧 Creating the user. 
## 🇵🇹 A criar o utilizador.
COPY config/global/etc/sudoers /etc/sudoers
RUN chmod 0440 /etc/sudoers

ARG USER_FOLDER=/home/$UUSER
ARG ZDOTDIR=$USER_FOLDER/.config/zsh

RUN [[ $(getent group $GROUP) ]] || groupadd $GROUP
RUN useradd -m $UUSER
RUN usermod -aG root $UUSER
RUN usermod -aG users $UUSER
RUN usermod -aG wheel $UUSER
RUN usermod -aG $GROUP $UUSER
RUN usermod -g root $UUSER
RUN usermod -u 1001 $UUSER
RUN usermod $UUSER -p "$(openssl passwd -1 piltover-and-zaun)"
RUN groups $UUSER

USER $UUSER
WORKDIR $USER_FOLDER

ENV PATH $USER_FOLDER/.local/bin:$USER_FOLDER/.rbenv/shims:$USER_FOLDER/.cargo/bin:/opt/miktex/bin:/usr/bin:$PATH

COPY --chown=$UUSER:$GROUP \
      config/downloads/minimal/aur/* \
      config/downloads/minimal/xbps/* \
      $USER_FOLDER/Transferências/
COPY --chown=$UUSER:$GROUP \
      config/global/home/username/.dircolors \
      config/global/home/username/.fzf.bash \
      config/global/home/username/.fzf.zst \
      config/global/home/username/.fzf.zsh \
      config/global/home/username/.znap.zst \
      config/global/home/username/.zprofile \
      config/global/home/username/.zshenv \
      $USER_FOLDER/
COPY --chown=$UUSER:$GROUP config/global/home/username/.config $USER_FOLDER/.config
COPY --chown=$UUSER:$GROUP config/global/home/username/.local/bin/colours $USER_FOLDER/.local/bin/

# 🇬🇧 SYSTEM
# 🇵🇹 SISTEMA
# =============================

### 🇬🇧 Returning as root
### 🇵🇹 A retornar como root.
USER root
WORKDIR $USER_FOLDER

COPY config/global/etc /etc

COPY --chown=$UUSER:$GROUP scripts/1-root $USER_FOLDER/.local/bin/
RUN 1-root

COPY --chown=$UUSER:$GROUP scripts/2-root-extra $USER_FOLDER/.local/bin/
RUN 2-root-extra

# 🇬🇧 USER
# 🇵🇹 UTILIZADOR
# =============================

### 🇬🇧 Returning as user
### 🇵🇹 A retornar como utilizador.
USER $UUSER
WORKDIR $USER_FOLDER

COPY --chown=$UUSER:$GROUP scripts/1-user $USER_FOLDER/.local/bin/
RUN 1-user

COPY --chown=$UUSER:$GROUP scripts/2-user-extra $USER_FOLDER/.local/bin/
RUN 2-user-extra

RUN sudo rm -rf /home/benegus/Transferências/*

COPY config/global/usr/share /usr/share

CMD ["zsh"]