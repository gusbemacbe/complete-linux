# Setup fzf
# ---------
if [[ ! "$PATH" == */home/gusbemacbe/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/gusbemacbe/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/gusbemacbe/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/gusbemacbe/.fzf/shell/key-bindings.bash"
