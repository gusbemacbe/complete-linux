# 🇬🇧 Java environment variables
# 🇵🇹 Variáveis de ambiente do Java
# 🇪🇸 Variables de entorno de Java
export JAVA_HOME=$HOME/Projetos/desenvolvimento-local/java/jdk8
export PATH=$JAVA_HOME/bin:$PATH

# 🇬🇧 Apache environment variables
# 🇵🇹 Variáveis de ambiente do Apache
# 🇪🇸 Variables de entorno de Apache
export APACHE_HOME=$HOME/Projetos/desenvolvimento-local/apache
export PATH=$APACHE_HOME/bin:$PATH

# 🇬🇧 Maven environment variables
# 🇵🇹 Variáveis de ambiente do Maven
# 🇪🇸 Variables de entorno de Maven
export M2_HOME=$HOME/Projetos/desenvolvimento-local/apache/maven

# 🇬🇧 JBoss environment variables
# 🇵🇹 Variáveis de ambiente do JBoss
# 🇪🇸 Variables de entorno de JBoss
export JBOSS_HOME=$HOME/Projetos/desenvolvimento-local/servers/jboss-datagrid-server
export PATH=$JBOSS_HOME/bin:$PATH

# 🇬🇧 ~/.local/bin environment variables
# 🇵🇹 Variáveis de ambiente do ~/.local/bin
# 🇪🇸 Variables de entorno de ~/.local/bin
export PATH=$HOME/.local/bin:$PATH
