FROM alpine:edge

LABEL distro "Ubuntu Linux"
LABEL description "Each split compressed file for Docker to pull with Git Bash and WSL limiting in maximun 1.5 GB"
LABEL tag "Compressed"
LABEL version "2.1.0"
LABEL maintainer "Gustavo Costa <gusbemacbe@gmail.com>"
LABEL vendor "Gustavo Costa"

COPY wsl/voidlinux/voidlinux-complete.tar.partae /home/

RUN ls -al /home/

CMD ["sh"]